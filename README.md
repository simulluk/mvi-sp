# Soutěž na Kaggle – PetFinder.my - Pawpularity Contest

## Podrobné zadání

Citace z popisu:

_In this competition, you’ll analyze raw images and metadata to predict the “Pawpularity” of pet photos. You'll train and test your model on PetFinder.my's thousands of pet profiles. Winning versions will offer accurate recommendations that will improve animal welfare._

Pro mne to znamená zkoumat roztomilost štěňátek a koťátek z obrázků, například pomocí konvoluční sítě.

## Pokyny k dotažení potřebných velkých dat

Nemám tušení, jak se k datům dostat bez potvrzení podmínek.

K nalezení jsou zde: [https://www.kaggle.com/c/petfinder-pawpularity-score/data](https://www.kaggle.com/c/petfinder-pawpularity-score/data)

[https://drive.google.com/drive/folders/1R9vqzEQxcEYn-NrtayqooCPZg546W6bs?usp=sharing](https://drive.google.com/drive/folders/1R9vqzEQxcEYn-NrtayqooCPZg546W6bs?usp=sharing) obsahuje
všechny notebooky, co jsou zároveň součástí repozitáře.

## Pokyny ke spuštění

Podobný problém jako výše, bez dat / mého veřejného notebooku to nebude ono.

Zde je můj veřejný notebook, 1. funkční varianta, nejhloupější varianta s průmerem: [https://www.kaggle.com/lsimulik/pawpets/notebook](https://www.kaggle.com/lsimulik/pawpets/notebook).

**Součástí repozitáře**, v adresáři _notebook_, je pět mých notebooků, které jsem odevzdal na kaggle:
- mean: Vrací vždy průměr
- conv: Konvoluční síť s 0,1 dropout
- conv_log: Konvoluční síť s 0,1 dropout a transformovanou predikovanou proměnnou
- sign: Malá neuronová síť pracující s _tagy_ obrázků, transformovaná predikovaná proměnná
- ensemble: Společná predikce _sign_ a *conv_log*
- ensemble-patient-extra-dense-bigger-dropout-no-log: ensemble s jinými parametry (netransformovaná vysvětlovaná proměnná, větší dropout, další FC vrstva)
- ensemble-patient-extra-dense-bigger-dropout: ensemble s jinými parametry (větší dropout, další FC vrstva)
- learning-to-resize: Malý pokus s learning to resize a DenseNet-121
- ensemble-two-cnn: Dvě konvoluční sítě, řešící každá svůj druh obrázku
